#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	//setWindowFlags(Qt::FramelessWindowHint);
	setMinimumSize(1366, 768);
	setWindowTitle("ArduScope v0.1");
	serialHandler = new SerialHandler(this);
	serialHandler->startSerial("/dev/ttyACM0", 2);
	scopeWidget = new ScopeWidget(this);
	connect(serialHandler, SIGNAL(scopeData(char)), scopeWidget, SLOT(recieve(char)));
	setCentralWidget(scopeWidget);
	ribbonWidget = new RibbonWidget(this);
	addToolBar(ribbonWidget);
	snapCheckBox = new QCheckBox("Snap To", this);
	snapComboBox = new QComboBox(this);
	snapComboBox->addItems({"Input 1", "Input 2", "Input 3", "Input 4"});
	connect(snapComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onSnapIndexChanged(int)));
	connect(snapCheckBox, SIGNAL(clicked(bool)), this, SLOT(onSnapClicked(bool)));
	initActions();
	initRibbon();

}

MainWindow::~MainWindow()
{

}


void MainWindow::onOpen()
{
	qInfo() << "onOpen";
}

void MainWindow::initActions()
{
	openAction = createAction("Open\nFile", getStdIcon(QStyle::StandardPixmap::SP_DialogOpenButton), "Open file from disk", true, QKeySequence::Open);
	connect(openAction, SIGNAL(triggered()), this, SLOT(onOpen()));
	saveAction = createAction("Save\nFile", getStdIcon(QStyle::StandardPixmap::SP_DialogSaveButton), "Save file to disk", true, QKeySequence::Save);
	startAction = createAction("Start", getStdIcon(QStyle::StandardPixmap::SP_MediaPlay), "Start sensor scope", true);
	connect(startAction, SIGNAL(triggered()), this, SLOT(onStart()));
	stopAction = createAction("Stop", getStdIcon(QStyle::StandardPixmap::SP_MediaStop), "Stop sensor scope", true);
	connect(stopAction, SIGNAL(triggered()), this, SLOT(onStop()));
	recordAction = createAction("Record", getStdIcon(QStyle::StandardPixmap::SP_ArrowRight), "Stop sensor scope", true);
	connect(recordAction, SIGNAL(triggered()), this, SLOT(onRecord()));
	snapAction = createAction("Snap\To", getStdIcon(QStyle::StandardPixmap::SP_ArrowUp), "Snap to input leading edge", false,QKeySequence::UnknownKey,true);

}

void MainWindow::onStart(){
	startAction->setEnabled(false);
	recordAction->setEnabled(false);
	serialHandler->startScope();
}

void MainWindow::onStop(){
	startAction->setEnabled(true);
	recordAction->setEnabled(true);
	serialHandler->stopScope();
}

void MainWindow::onRecord(){
	startAction->setEnabled(false);
	recordAction->setEnabled(false);
}
int snapIndex = 0;
void MainWindow::onSnapIndexChanged(int value)
{
	qInfo() << value;
	snapIndex = value;
	scopeWidget->setSnapToPath(snapIndex);
}

void MainWindow::onSnapClicked(bool value)
{
	snapComboBox->setEnabled(value);
	scopeWidget->setSnapToPath(value ? snapIndex : -1);
}

void MainWindow::initRibbon()
{
	RibbonTab *homeTab = this->ribbonWidget->createRibbonTab("Home");
	RibbonPane *filePane = homeTab->createRibbonPane("File");
	filePane->addRibbonWidget(new RibbonButton(filePane, openAction, true));
	filePane->addRibbonWidget(new RibbonButton(filePane, saveAction, true));
	RibbonTab *settingsTab = this->ribbonWidget->createRibbonTab("Settings");
	RibbonPane *scopePane = homeTab->createRibbonPane("Scope");
	scopePane->addRibbonWidget(new RibbonButton(scopePane, startAction, true));
	scopePane->addRibbonWidget(new RibbonButton(scopePane, stopAction, true));
	scopePane->addRibbonWidget(new RibbonButton(scopePane, recordAction, true));
	auto wg = scopePane->createWidgetGrid(260);


	wg->addWidget(snapCheckBox);
	wg->addWidget(snapComboBox);
}

QIcon MainWindow::getStdIcon(QStyle::StandardPixmap stdpm)
{
	return QIcon(style()->standardIcon(stdpm));
}

QAction * MainWindow::createAction(QString caption, QIcon icon, QString tip, bool iconVisible, QKeySequence::StandardKey shortcut, bool checkable)
{
	QAction *action;
	if (iconVisible) action = new QAction(icon, caption, this);
	else action = new QAction(caption, this);
	action->setStatusTip(tip);
	action->setIconVisibleInMenu(iconVisible);
	if (shortcut)
		action->setShortcuts(shortcut);
	action->setCheckable(checkable);
	return action;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	serialHandler->stopScope();
	QThread::msleep(50);
	//event->ignore();
}
