#ifndef SERIALHANDLER_H
#define SERIALHANDLER_H

#include <QTextStream>
#include <QByteArray>
#include <QThread>
#include <QMutex>

enum SerialMessageState {
	None,
	Init,
	SetConfig,
	ErrorMessage,
	InfoMessage,
	ScopeData,
	Stop
};

class SerialHandler : public QThread
{
	Q_OBJECT

	QString m_portName;
	QByteArray m_writeData;
	QByteArray m_readData;
	int m_waitTimeout = 0;
	QMutex m_mutex;
	bool m_quit = false;
	SerialMessageState m_sms;
	QByteArray m_textMessage;

	void run() override;
	void parse(char d);

	void parseMessageType(char d);
	void handleTextMessage(char d);
public:
	explicit SerialHandler(QObject *parent = nullptr);
	~SerialHandler();
	void startSerial(const QString &portName, int waitTimeout);
	void send(QByteArray data);
	void stopScope();
	void startScope();

signals:
	void data();
	void error(const QString &s);
	void timeout(const QString &s);
	void errorMessage(const QString &s);
	void infoMessage(const QString &s);
	void scopeData(char sd);

private slots:
	void onData();
};

#endif // SERIALHANDLER_H
