#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAction>
#include <QCheckBox>
#include <QComboBox>
#include <QMainWindow>
#include <QStyle>
#include <ribbonwidget.h>
#include <scopewidget.h>
#include <serialhandler.h>


class MainWindow : public QMainWindow
{
	Q_OBJECT

	ScopeWidget *scopeWidget;
	SerialHandler *serialHandler;
	RibbonWidget *ribbonWidget;
	QCheckBox *snapCheckBox;
	QComboBox *snapComboBox;

	QAction *openAction;
	QAction *saveAction;
	QAction *startAction;
	QAction *stopAction;
	QAction *recordAction;
	QAction *snapAction;

	void initActions();
	void initRibbon();
	QIcon getStdIcon(QStyle::StandardPixmap stdpm);
	QAction * createAction(QString caption, QIcon icon, QString tip, bool iconVisible, QKeySequence::StandardKey shortcut=QKeySequence::StandardKey::UnknownKey, bool checkable=false);
	void closeEvent(QCloseEvent *event) override;
public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();	

private slots:
	void onOpen();
	void onStart();
	void onStop();
	void onRecord();
	void onSnapIndexChanged(int value);
	void onSnapClicked(bool value);
};
#endif // MAINWINDOW_H
