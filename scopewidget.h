#ifndef SCOPEWIDGET_H
#define SCOPEWIDGET_H

#include <QList>
#include <QPen>
#include <QTimer>
#include <QWidget>

class ScopePath {
	QLineF *lines1;
	QLineF *lines2;
	QPen pen;
	int pathIndex = 1;
	int index = 0;
	int lineCount = 2500;
	double scale = 0.05;
	int snapIndex = 1;
	bool snap = false;

	void increment();
	QLineF *getPath();
public:
	explicit ScopePath(QColor color);
	void paint(QPainter &painter, double penWidth);
	void setIncrementedValue(double value);
	void setPenWidth(float value);
	void setSnap(bool value);
	int getSnapIndex();
	void setSnapIndex(int value);
};

class ScopeWidget : public QWidget
{
	Q_OBJECT
private:
	QPen gridPen;
	QSize gridCount;
	QTimer *updateTimer;
	QList<ScopePath*> paths;
	int snapIndex = 0;
	int snapToPath = -1;
	//ScopePath *path1;
	//ScopePath *path2;
	//ScopePath *path3;
	//ScopePath *path4;

	void drawGrid(QPainter &painter);
	void drawData(QPainter &painter);
public:
	explicit ScopeWidget(QWidget *parent = nullptr);
	virtual void paintEvent(QPaintEvent *pe);
	void setSnapToPath(int value);
signals:

public slots:
	void recieve(char data);
	void onUpdate();

};

#endif // SCOPEWIDGET_H
