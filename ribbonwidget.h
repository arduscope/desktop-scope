#ifndef RIBBONWIDGET_H
#define RIBBONWIDGET_H

#include <QTabWidget>
#include <QToolBar>
#include <QWidget>
#include <QGridLayout>
#include <QPaintEvent>
#include <QToolButton>

class RibbonButton: public QToolButton {
	Q_OBJECT

	QAction *action;
public:
	explicit RibbonButton(QWidget *parent, QAction *action, bool isLarge);

public slots:
	void updateFromAction();
};

class RibbonSeparator: public QWidget {
	Q_OBJECT
public:
	explicit RibbonSeparator(QWidget *parent);
	virtual void paintEvent(QPaintEvent *e);
};

class RibbonPane : public QWidget {
	Q_OBJECT
	QHBoxLayout *cl;
public:
	explicit RibbonPane(QWidget *parent, QString name);
	void * addRibbonWidget(QWidget *widget);
	QGridLayout * createWidgetGrid(int width);
};

class RibbonTab : public QWidget {
	Q_OBJECT
public:
	explicit RibbonTab(QWidget *parent, QString name);
	RibbonPane * createRibbonPane(QString name);
};

class RibbonWidget : public QToolBar
{
	Q_OBJECT
	QTabWidget *tabWidget;

public:
	explicit RibbonWidget(QWidget *parent = nullptr);
	RibbonTab *createRibbonTab(QString name);

signals:

};

#endif // RIBBONWIDGET_H
