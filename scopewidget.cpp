#include "scopewidget.h"
#include <QPainter>
#include <QtMath>
#include <QDebug>

ScopeWidget::ScopeWidget(QWidget *parent)
	: QWidget{parent}
{
	paths.append(new ScopePath(QColor(100,140,255,150)));
	paths.append(new ScopePath(QColor(255,100,100,150)));
	paths.append(new ScopePath(QColor(100,255,150,150)));
	paths.append(new ScopePath(QColor(255,255,100,150)));
	gridCount.setHeight(9);
	gridCount.setWidth(16);
	gridPen.setColor(QColor(128,128,128, 80));
	gridPen.setWidth(2);
	updateTimer = new QTimer(this);
	connect(updateTimer, SIGNAL(timeout()), this, SLOT(onUpdate()));
	updateTimer->start(16);
}

void ScopeWidget::drawGrid(QPainter &painter)
{
	painter.setPen(gridPen);
	double h = size().height();
	double dh = h / (gridCount.height());
	double w = size().width();
	double dw = w / (gridCount.width());

	for (int i = 0; i < gridCount.height(); i++)
		painter.drawLine(QLineF(0.0, dh * (i+0.5), w, dh * (i+0.5)));
	for (int i = 0; i < gridCount.width(); i++)
		painter.drawLine(QLineF(dw*(i+0.5), 0.0, dw*(i+0.5), h));
}

void ScopeWidget::drawData(QPainter &painter)
{
	painter.save();

	painter.translate(width(),height()/2.0);
	painter.scale(-width(), -width());
	painter.translate(0,-0.2);
	for (int i = 0; i< paths.length(); i++){
		paths[i]->paint(painter, 2/width());
		painter.translate(0,0.1);
	}
	painter.restore();
}

void ScopeWidget::paintEvent(QPaintEvent *pe)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::RenderHint::Antialiasing);
	this->drawData(painter);
	this->drawGrid(painter);
	painter.end();
}

void ScopeWidget::setSnapToPath(int value)
{
	if (snapToPath!=-1)	paths[snapToPath]->setSnap(false);
	snapToPath = value;
	if (snapToPath!=-1)	paths[snapToPath]->setSnap(true);
}

void ScopeWidget::recieve(char data)
{
	for (int i = 0; i< paths.length(); i++){
		double p = data & (1<<i) ? 1.0 : 0.0;
		paths[i]->setIncrementedValue(p);
		if (snapToPath != i && snapToPath != -1){
			paths[i]->setSnapIndex(snapIndex);
		}
	}
	if (snapToPath != -1){
		snapIndex = paths[snapToPath]->getSnapIndex();
	}
}

void ScopeWidget::onUpdate()
{
	update();
}

void ScopePath::increment()
{
	index += 1;
	if (index >= lineCount){
		index = 0;
		pathIndex = pathIndex == 1 ? 2 : 1;
	}
}

QLineF *ScopePath::getPath()
{
	if (pathIndex == 1) return lines1;
	else return lines2;
}


ScopePath::ScopePath(QColor color)
{
	double dw = 1.0/(double)lineCount;
	QPointF fp1(0, 0.1);
	QPointF fp2(0, 0.1);
	QPointF lp1(dw, 0.1);
	QPointF lp2(dw, 0.1);
	lines1 = new QLineF[lineCount];
	lines2 = new QLineF[lineCount];
	lines1[0] = QLineF(fp1, lp1);
	lines2[0] = QLineF(fp2, lp2);
	pen.setColor(color);
	pen.setWidthF(2.0/1000.0);

	for (int i = 1; i < lineCount; i++){
		QPointF np1(i*dw, 0.1);
		QPointF np2(i*dw, 0.1);
		lines1[i] = QLineF(lp1, np1);
		lines2[i] = QLineF(lp2, np2);
		lp1 = np1;
		lp2 = np2;
	}
}

void ScopePath::setPenWidth(float value){
	pen.setWidthF(value);
}

void ScopePath::setSnap(bool value)
{
	snap = value;
}

int ScopePath::getSnapIndex()
{
	return snapIndex;
}

void ScopePath::setSnapIndex(int value)
{
	snapIndex = value;
}

void ScopePath::paint(QPainter &painter, double penWidth)
{
	painter.save();
	pen.setWidthF(penWidth);
	painter.setPen(pen);
	painter.translate(QPointF(1.0-(double)snapIndex/(double)lineCount, 0));
	if (pathIndex == 1) painter.drawLines(lines1, lineCount);
	else painter.drawLines(lines2, lineCount);
	painter.translate(-1,0);
	if (pathIndex == 1) painter.drawLines(lines2, lineCount);
	else painter.drawLines(lines1, lineCount);
	painter.restore();
}

void ScopePath::setIncrementedValue(double value)
{
	increment();
	QLineF *path = getPath();
	QPointF p1;
	if (index==0){
		if (pathIndex == 1){
			p1 = path[index].p1();
			p1.setY(lines2[lineCount-1].y2());
		} else {
			p1 = path[index].p1();
			p1.setY(lines1[lineCount-1].y2());
		}
	}else{
		p1 = path[index-1].p2();
	}
	QPointF p2 = path[index].p2();
	p2.setY(value*scale);
	path[index].setP1(p1);
	path[index].setP2(p2);

	if (p1.y()!=0 && p2.y()==0) {
		snapIndex = index;
	}
	if (!snap) snapIndex = index;
}
