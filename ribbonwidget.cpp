#include "ribbonwidget.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QScreen>

double guiScale = .96;

RibbonButton::RibbonButton(QWidget *parent, QAction *action, bool isLarge)
	: QToolButton{parent}
{
	this->action = action;
	updateFromAction();
	connect(this, SIGNAL(clicked()), action, SLOT(trigger()));
	connect(action, SIGNAL(changed()), this, SLOT(updateFromAction()));
	setStyleSheet("QToolButton { border: 1px solid transparent; margin: 2px 2px 2px 2px;} QToolButton:hover,QToolButton:checked { border: 1px solid rgba(120,160,255,20%); background-color: rgba(120,160,255,10%)} QToolButton:pressed { border: 1px solid rgba(120,160,255,20%); background-color: rgba(120,160,255,20%)}");
	if (isLarge){
		setIconSize(QSize(32*guiScale, 32*guiScale));
		setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextUnderIcon);
	} else {
		setIconSize(QSize(16*guiScale, 16*guiScale));
		setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextBesideIcon);
	}
}

void RibbonButton::updateFromAction()
{
	setText(action->text());
	setStatusTip(action->statusTip());
	setToolTip(action->toolTip());
	setIcon(action->icon());
	setEnabled(action->isEnabled());
	setCheckable(action->isCheckable());
	setChecked(action->isChecked());
}

RibbonSeparator::RibbonSeparator(QWidget *parent)
	: QWidget{parent}
{
	setMinimumHeight(90*guiScale);
	setMinimumWidth(1);
	setMaximumWidth(1);

}

void RibbonSeparator::paintEvent(QPaintEvent *e)
{
	QPainter *painter = new QPainter();
	painter->begin(this);
	painter->fillRect(e->rect(), QColor(0,0,0,40));
	painter->end();
}


RibbonPane::RibbonPane(QWidget *parent, QString name)
	: QWidget{parent}
{
	//setStyleSheet(getStyleSheet("ribbonPane"));
	QHBoxLayout *hl = new QHBoxLayout(this);
	hl->setSpacing(0);
	hl->setContentsMargins(0,0,0,0);
	setLayout(hl);
	QWidget *vw = new QWidget(this);
	hl->addWidget(vw);
	hl->addWidget(new RibbonSeparator(this));
	QVBoxLayout *vl = new QVBoxLayout(vw);
	vl->setSpacing(0);
	vl->setContentsMargins(0,0,0,0);
	vw->setLayout(vl);
	QLabel *label = new QLabel(vw);
	label->setMaximumHeight(20);
	label->setText(name);
	label->setAlignment(Qt::AlignmentFlag::AlignCenter);
	label->setStyleSheet("color:#999; background:#26888888; font-weight: bold; font-size: 14px;");
	label->setContentsMargins(5,0,5,0);
	QWidget *cw = new QWidget(vw);
	vl->addWidget(cw);
	vl->addWidget(label);
	cl = new QHBoxLayout(cw);
	cl->setAlignment(Qt::AlignmentFlag::AlignLeft);
	cl->setSpacing(0);
	cl->setContentsMargins(0,0,0,0);
	cw->setLayout(cl);
}

void *RibbonPane::addRibbonWidget(QWidget *widget)
{
	cl->addWidget(widget, 0, Qt::AlignmentFlag::AlignTop);
}

QGridLayout *RibbonPane::createWidgetGrid(int width)
{
	QWidget *w = new QWidget(this);
	QGridLayout *gl = new QGridLayout(w);
	w->setMaximumWidth(width);
	w->setLayout(gl);
	gl->setSpacing(0);
	gl->setContentsMargins(0,0,0,0);
	cl->addWidget(w);
	cl->setAlignment(Qt::AlignmentFlag::AlignTop|Qt::AlignmentFlag::AlignLeft);
	return gl;
}

RibbonTab::RibbonTab(QWidget *parent, QString name)
	: QWidget{parent}
{
	setObjectName("tab_" + name);
	QHBoxLayout *l = new QHBoxLayout(this);
	setLayout(l);
	l->setContentsMargins(0,0,0,0);
	l->setSpacing(0);
	l->setAlignment(Qt::AlignmentFlag::AlignLeft);
}

RibbonPane *RibbonTab::createRibbonPane(QString name)
{
	RibbonPane *rp = new RibbonPane(this, name);
	layout()->addWidget(rp);
	return rp;
}


RibbonTab *RibbonWidget::createRibbonTab(QString name)
{
	RibbonTab * ribbonTab = new RibbonTab(this, name);
	tabWidget->addTab(ribbonTab, name);
	return ribbonTab;
}

RibbonWidget::RibbonWidget(QWidget *parent)
	: QToolBar{parent}
{
	QScreen *screen = QApplication::screens().at(0);
	guiScale = screen->logicalDotsPerInch()/96;
	setObjectName("ribbonWidget");
	setWindowTitle("Ribbon");
	tabWidget = new QTabWidget(this);
	setMovable(false);
	addWidget(tabWidget);
	toggleViewAction()->setEnabled(false);
}
