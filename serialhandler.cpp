#include "serialhandler.h"
#include <QSerialPort>
#include <QTime>
#include <QDebug>

#define READYBYTE 1
#define TYPEBYTE 2
#define DATABYTE 3
#define ENDBYTE 4

SerialHandler::SerialHandler(QObject *parent) :
	QThread(parent)
{
	m_sms = SerialMessageState::None;
	connect(this, SIGNAL(data()), this, SLOT(onData()));
}

//! [0]
SerialHandler::~SerialHandler()
{
	m_mutex.lock();
	m_quit = true;
	m_mutex.unlock();
	wait();
}

void SerialHandler::startSerial(const QString &portName, int waitTimeout)
{
	const QMutexLocker locker(&m_mutex);
	m_portName = portName;
	m_waitTimeout = waitTimeout;

	if (!isRunning())
		start();
}

void SerialHandler::send(QByteArray data)
{
	m_mutex.lock();
	m_writeData += data;
	m_mutex.unlock();
}

void SerialHandler::onData()
{
	m_mutex.lock();
	QByteArray data = m_readData;
	m_readData.clear();
	m_mutex.unlock();
	for (int i = 0; i < data.length(); i++){
		parse(data.data()[i]);
	}
}

void SerialHandler::run()
{
	bool currentPortNameChanged = false;

	m_mutex.lock();
	QString currentPortName;
	QByteArray currentWriteData;
	if (currentPortName != m_portName) {
		currentPortName = m_portName;
		currentPortNameChanged = true;
	}
	int currentWaitTimeout = m_waitTimeout;
	m_mutex.unlock();

	QSerialPort serial;
	serial.setBaudRate(500000);

	while (!m_quit) {
		if (currentPortNameChanged) {
			serial.close();
			serial.setPortName(currentPortName);

			if (!serial.open(QIODevice::ReadWrite)) {
				emit error(tr("Can't open %1, error code %2")
									 .arg(m_portName).arg(serial.error()));
				return;
			} else {
				qInfo() << "Serial connected";
			}
		}

		if (serial.waitForReadyRead(currentWaitTimeout)) {
			QByteArray readData = serial.readAll();
			//qInfo() << QString(readData);
			m_mutex.lock();
			m_readData += readData;
			m_mutex.unlock();
			emit data();
		}

		if (currentWriteData.length()>0){
			serial.write(currentWriteData);
			if (serial.waitForBytesWritten(m_waitTimeout)) {
				currentWriteData.clear();
			} else {
				emit timeout(tr("Wait write response timeout %1").arg(QTime::currentTime().toString()));
			}
		}
		//! [9]  //! [13]
		m_mutex.lock();
		if (currentPortName != m_portName) {
			currentPortName = m_portName;
			currentPortNameChanged = true;
		} else {
			currentPortNameChanged = false;
		}
		currentWaitTimeout = m_waitTimeout;
		currentWriteData += m_writeData;
		m_writeData.clear();
		m_mutex.unlock();
	}
}

void SerialHandler::parse(char d)
{
	//qInfo() << "sms: " << sms;
	switch (m_sms){
		case None:
			parseMessageType(d);
		break;
		case ScopeData:
			emit scopeData(d);
			m_sms = None;
		break;
		case ErrorMessage:
		case InfoMessage:
			handleTextMessage(d);
		break;
		default:
			m_sms = None;
	}
}

void SerialHandler::parseMessageType(char d){
	switch (d){
		case ScopeData:
			m_sms = (SerialMessageState)d;
		break;
		case Init:
			startScope();
		break;
		case ErrorMessage:
		case InfoMessage:
			m_textMessage.clear();
			m_sms = (SerialMessageState)d;
		break;
	}
}


void SerialHandler::handleTextMessage(char d){
	if (d==0){
		if (m_sms == ErrorMessage) emit errorMessage(QString(m_textMessage));
		else emit infoMessage(QString(m_textMessage));
		qInfo() << "Arduino Message: " << QString(m_textMessage);
		m_sms = None;
		return;
	}
	m_textMessage += d;
}

void SerialHandler::startScope(){
	qInfo() << "startScope";
	QByteArray initData;
	initData += READYBYTE;
	initData += TYPEBYTE;
	initData += (char)Init;
	initData += ENDBYTE;
	send(initData);
}

void SerialHandler::stopScope()
{
	QByteArray stopData;
	stopData += READYBYTE;
	stopData += TYPEBYTE;
	stopData += (char)Stop;
	stopData += ENDBYTE;
	send(stopData);
	qInfo() << "Stopping serial" << stopData;
}
